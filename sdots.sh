#!/usr/bin/env bash
SCRIPT_DIR="$(cd $(dirname $0) && pwd)"

echo "==========================================================================="
echo "                            Installing Dotfiles                            "
echo "---------------------------------------------------------------------------"

echo "---------------------------------------------------------------------------"
echo "                Checking for existing files and directories                "
echo "---------------------------------------------------------------------------"

# User Config Directory
USER_CONFIG_DIR="~/.config"

echo "Deleting existing directories"
# Folder locations
BTOP_DIR="${USER_CONFIG_DIR}/btop"
GIT_DIR="${USER_CONFIG_DIR}/git"
HTOP_DIR="${USER_CONFIG_DIR}/htop"
LF_DIR="${USER_CONFIG_DIR}/lf"
MICRO_DIR="${USER_CONFIG_DIR}/micro"
NEOFETCH_DIR="${USER_CONFIG_DIR}/neofetch"
TMUX_DIR="${USER_CONFIG_DIR}/tmux"
ZSH_DIR="${USER_CONFIG_DIR}/zsh"
# Check for existing folders and delete them
[[ ! -d ${BTOP_DIR} ]] && rm -rf ${BTOP_DIR}
[[ ! -d ${GIT_DIR} ]] && rm -rf ${GIT_DIR}
[[ ! -d ${HTOP_DIR} ]] && rm -rf ${HTOP_DIR}
[[ ! -d ${LF_DIR} ]] && rm -rf ${LF_DIR}
[[ ! -d ${MICRO_DIR} ]] && rm -rf ${MICRO_DIR}
[[ ! -d ${NEOFETCH_DIR} ]] && rm -rf ${NEOFETCH_DIR}
[[ ! -d ${TMUX_DIR} ]] && rm -rf ${TMUX_DIR}
[[ ! -d ${ZSH_DIR} ]] && rm -rf ${ZSH_DIR}

echo "---------------------------------------------------------------------------"
echo "                             Linking Dotfiles                              "
echo "---------------------------------------------------------------------------"

echo "Linking .config directory file"
# Folders
ln -sf ${SCRIPT_DIR}/.config/btop ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/git ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/htop ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/lf ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/micro ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/neofetch ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/tmux ${USER_CONFIG_DIR}
ln -sf ${SCRIPT_DIR}/.config/zsh ${USER_CONFIG_DIR}
# Files
ln -sf ${SCRIPT_DIR}/.config/VSCode/settings.json ${USER_CONFIG_DIR}/Code\ -\ OSS/Users/

echo "Linking HOME directory files"
ln -sf ${SCRIPT_DIR}/.config/bash/.bashrc $HOME
ln -sf ${USER_CONFIG_DIR}/zsh/.zshenv $HOME
ln -sf ${USER_CONFIG_DIR}/.p10k.zsh $HOME
ln -sf ${USER_CONFIG_DIR}/.p10k.tty.zsh $HOME
ln -sf ${USER_CONFIG_DIR}/tmux/.tmux.conf $HOME
ln -sf ${USER_CONFIG_DIR}/tmux/.tmux.conf.local $HOME

echo "---------------------------------------------------------------------------"
echo "                            Making Directories                             "
echo "---------------------------------------------------------------------------"

echo "Making history directory"
# Folder location
history_DIR="$HOME/.cache/history"
zsh_DIR="$HOME/.cache/zsh"
# Check for existing folder and create it
[[ ! -d ${history_DIR} ]] && mkdir -p ${history_DIR}
[[ ! -d ${zsh_DIR} ]] && mkdir -p ${zsh_DIR}

echo "Making pkgs directory"
# Folder location
pkgs_DIR="$HOME/pkgs"
# Check for existing folder and create it
[[ ! -d ${pkgs_DIR} ]] && mkdir -p ${pkgs_DIR}

echo "---------------------------------------------------------------------------"
echo "                         Running additional Scripts                        "
echo "---------------------------------------------------------------------------"

./${SCRIPT_DIR}/Scripts/code-extensions.sh

echo "---------------------------------------------------------------------------"
echo "                          Installing PowerLevel10K                         "
echo "---------------------------------------------------------------------------"

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.powerlevel10k

echo "---------------------------------------------------------------------------"
echo "                            Installing NerdFonts                           "
echo "---------------------------------------------------------------------------"

sudo mkdir /usr/share/fonts/NerdFonts
sudo cp JetBrains\ Mono\ Regular\ Nerd\ Font\ Complete.ttf /usr/share/fonts/NerdFonts
sudo cp JetBrains\ Mono\ Regular\ Nerd\ Font\ Complete\ Mono.ttf /usr/share/fonts/NerdFonts

echo "---------------------------------------------------------------------------"
echo "                Finished, re-log for changes to take effect                "
echo "==========================================================================="
