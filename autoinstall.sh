#!/usr/bin/env bash

# Update the OS
sudo apt update && sudo apt -y upgrade

# Install GIT
sudo apt install -y git 

# Download and Install Server Dotfiles
cd ~ && git clone --depth=1 https://gitlab.com/anma.ai/sdots.git ".sdots" && cd ~/.sdots && ./Scripts/ubuntu.sh && ./sdots.sh
