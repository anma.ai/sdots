#!/usr/bin/env bash

code --install-extension aaron-bond.better-comments
code --install-extension anseki.vscode-color
code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension dracula-theme.theme-dracula
code --install-extension eamodio.gitlens
code --install-extension esbenp.prettier-vscode
code --install-extension formulahendry.auto-close-tag
code --install-extension formulahendry.auto-complete-tag
code --install-extension formulahendry.code-runner
code --install-extension mhutchie.git-graph
code --install-extension ms-vscode.hexeditor
code --install-extension naumovs.color-highlight
code --install-extension oderwat.indent-rainbow
code --install-extension shardulm94.trailing-spaces
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension vscode-icons-team.vscode-icons
code --install-extension wayou.vscode-todo-highlight