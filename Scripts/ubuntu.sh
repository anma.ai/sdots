#!/usr/bin/env bash

echo "Updating..."
# Update & Upgrade
sudo apt update && sudo apt -y upgrade

echo "Installing applications"
# Applications
sudo apt install -y micro htop neofetch zsh
snap install -y btop lsd

echo "Installing Terminal profile"
dconf load /org/gnome/terminal/legacy/profiles:/ < ~/.sdots/gnome-terminal-profiles.dconf

echo "Installing Powerlevel10k"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.powerlevel10k

echo "Done!"
