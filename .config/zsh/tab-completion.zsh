# Tab Completion
autoload -U colors && colors
autoload -U compinit && compinit -u

setopt auto_menu
setopt complete_in_word
setopt always_to_end

zstyle ':completion:*' menu select
# Auto complete with case insensitivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
# For all completions: Selected item color
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS} 'ma=0\;30;44'
# For all completions: Commands color
zstyle ':completion:*:commands' list-colors '=*=1;33'
# For all completions: Options color
zstyle ':completion:*:options' list-colors '=^(-- *)=36'
zmodload zsh/complist
compinit
_comp_options+=(globdots) #Hidden files
# Use caching so that commands like apt and dpkg complete are useable
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path ~/.cache/zsh/tab-cache
