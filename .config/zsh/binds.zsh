# VI mode
#bindkey -v
#export KEYTIMEOUT=1

# Use vim keys in tab completion menu
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'right' vi-forward-char

# Fix backspace bug when switching modes
#bindkey "^?" backward-delete-char

# Change directory with LF
bindkey -s "^o" "lfcd\n"

# Home, End, Delete keybinds
bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line
bindkey '\e[3~' delete-char
