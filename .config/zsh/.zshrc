#############################################################################################
#                                                                                           #
#                                           Theme                                           #
#                                                                                           #
#############################################################################################

# Powerlevel10k Theming
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

ZSH_THEME="powerlevel10k/powerlevel10k"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
#[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

if zmodload zsh/terminfo && (( terminfo[colors] >= 256 )); then
  [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
else
  [[ ! -f ~/.p10k.tty.zsh ]] || source ~/.p10k.tty.zsh
fi

# Dracula colors for zsh-syntax-highlighing
source ${ZDOTDIR}/colors/dracula-zsh-syntax-highlighting.sh

# Dracula TTY Colors
source ${ZDOTDIR}/colors/dracula-tty.sh

#############################################################################################
#                                                                                           #
#                                            Main                                           #
#                                                                                           #
#############################################################################################

# General Settings
setopt AUTO_CD

# History Settings
source ${ZDOTDIR}/history.zsh

# Tab Completion Settings
source ${ZDOTDIR}/tab-completion.zsh

# Source Functions
source ${ZDOTDIR}/functions.zsh

# Source Keybinds
source ${ZDOTDIR}/binds.zsh

#############################################################################################
#                                                                                           #
#                                          Aliases                                          #
#                                                                                           #
#############################################################################################

# General
alias ..='cd ..; ls'
alias ..=' cd ..; ls'
alias ...=' cd ..; cd ..; ls'
alias ....=' cd ..; cd ..; cd ..; ls'
alias src='source ${ZDOTDIR}/.zshrc'
alias zshrc='${=EDITOR} ${ZDOTDIR}/.zshrc'
alias zshp10k='${=EDITOR} ~/.p10k.zsh'
alias m='micro'
alias grep='grep --color=auto'
alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS}'
alias ip='ip -c'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L="| less"
alias -g M="| most"
alias btw='neofetch'
alias code-extensions='code --list-extensions | xargs -L 1 echo code --install-extension'

# System
alias df='df -Tha --total'
alias du='du -ach | sort -h'
alias free='free -mtl'
alias p='procs'
alias pw='p -w'
alias kill='killall -q'
alias mypip='curl http://ifconfig.me/; echo'
alias mypipinfo='curl https://ipinfo.io/json/; echo'
alias ping='ping -c 5'
alias cpuinfo='lscpu'
alias agpu='glxinfo | grep "OpenGL renderer"'
alias ccram="ClearRamCache"

# History, Search history, Case sensative history
alias h="fc -fl"
alias hs="fc -fl 0 | grep"
alias hsi="fc -fl 0 | grep -i"

# EXA
alias ls='exa --icons --color always --group-directories-first'
alias la='exa --icons --color always --group-directories-first -a'
alias ll='exa --icons --color always --group-directories-first --git -hbl'
alias lla='exa --icons --color always --group-directories-first --git -hbla'
alias llg='exa --icons --color always --group-directories-first --git -hblag'
alias lls='exa --icons --color always --group-directories-first --git -hblas size'
alias lld='exa --icons --color always --group-directories-first --git -hblaUum'
alias lt='exa --icons --color always --group-directories-first -T'
alias lta='exa --icons --color always --group-directories-first -Ta'

# LS Deluxe
#alias ls='lsd --group-dirs=first'
#alias la='ls -A --group-dirs=first'
#alias ll='ls -l --group-dirs=first --size=short'
#alias lla='ls -lA --group-dirs=first --size=short'
#alias lls='lla -S'
#alias llm='lla --date=relative'
#alias lt='ls --tree --group-dirs=first'
#alias lta='lt -A'

# TMUX
alias t='tmux'
alias tn='tmux new'
alias tns='tmux new -s'
alias tks='tmux kill-session -t'
alias tka='tmux kill-session -a'
alias tls='tmux list-sessions'
alias ta='tmux attach'
alias tas='tmux attach -t'
alias tlsk='tmux list-keys'
alias ti='tmux info'

# SSH
alias sshrsa='ssh-keygen -t rsa -b 4096'
alias sshag='eval "$(ssh-agent -s)"'

#############################################################################################
#                                                                                           #
#                                          Plugins                                          #
#                                                                                           #
#############################################################################################

source ~/.powerlevel10k/powerlevel10k.zsh-theme
source ${ZDOTDIR}/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ${ZDOTDIR}/plugins/zsh-command-not-found/command-not-found.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-you-should-use/you-should-use.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ${ZDOTDIR}/plugins/zsh-dirhistory/dirhistory.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-copyfile/copyfile.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-copydir/copydir.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-extract/extract.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-sudo/sudo.plugin.zsh
source ${ZDOTDIR}/plugins/zsh-git/git.plugin.zsh