Server Dotfiles
---

### Installation

To install the dotfiles, copy and paste the command below into your terminal.  This will update and upgrade the system, install git, micor, htop, neofetch, btop, lsd as well as the dotfiles to the correct location.

```
bash <(curl -sS https://gitlab.com/anma.ai/sdots/-/raw/master/autoinstall.sh)
```
